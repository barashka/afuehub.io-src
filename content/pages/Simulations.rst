=================
Simulations
=================

:order: 3

Computer simulations help to study complex systems, where doing experiments
would be too difficult, too expensive or simply impossible. Typical application
are wheaten forecast, climatology, astrophysics, fluid dynamics, evolution,
sociology or economics. Different types are used, such as Monte Carlo
simulations for probabilistic systems, grid-based codes to solve differential
equation in simplified geometries or n-body codes.

For my work, I mostly use N-body and grid simulations (or hybrids), however I
also did some Monte Carlo calculations. The engineering problems I worked on
involved the simulation of the wing flow of the `Ray VTOL aircraft
<http://rayaircraft.com>`_ with `ANSYS Fluent
<https://www.ansys.com/Products/Fluids/ANSYS-Fluent>`_ and finite elements
analyses on nano/micro satellites for static load, modal analysis and dynamic
response with `CATIA <https://www.3ds.com/products-services/catia/>`_.

In astrophysics, I did some galactic dynamics using `Gadget-2
<https://wwwmpa.mpa-garching.mpg.de/gadget/>`_ and `RAMSES
<http://www.ics.uzh.ch/~teyssier/ramses/RAMSES.html>`_. For my `PhD
<{filename}../pdfs/AndreasFueglistaler_PhD.pdf>`_ I studied the dynamics of
molecular hydrogen in the interstellar medium with `LAMMPS
<http://lammps.sandia.gov>`_, combining an N-body approach for the
intermolecular potential with a hybrid particle-grid-based approach (P³M) for
gravity. I simulated hydrogen in a Jeans collapse and phase transition, leading
to the formation of solid hydrogen.
