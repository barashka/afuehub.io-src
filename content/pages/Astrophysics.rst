============
Astrophysics
============

:order: 2

I study the formation of cold, substellar sized bodies due to the fragmentation
of gravitationally unstable fluids in a phase transition. The main interest is
the potential formation of rocky, substellar H₂ bodies in cold fluids such as
molecular clouds and disks. However, the combination of phase transition and
gravity may be relevant for a wider range of astrophysical situations, such as
proto-planetary disks.

Motivation
-----------

The Big Bang nucleosynthesis predicts a much larger amount of baryonic matter
than observed in the low-*z* universe. Cold H₂ bodies would be an ideal
candidate for the missing baryons in galaxies as they are hardly detectable. In
very cold dark gas regions such bodies can form as H₂ may be in a phase
transition. The formation can also happen during the collapsing phase of star
formation or in cold, substellar structures such as cometary knots. The comets
observed in our solar system may be remnants of much larger H₂ bodies, as the
volatile H₂ would vaporize before being close enough to the sun for detection.

Context
--------

Molecular clouds consist of 3/4 H₂ and 1/4 He. Observations of various ices and
the existence of comets suggest that phase transition processes are happening
in cold regions. H₂ can be in a phase transition below its critical temperature
(33 K) if the density is high enough. H₂ condensation conditions can be reached
during plane-parallel contraction, which is the fastest collapsing geometry and
where the temperature rise is only of a factor 2. A H₂ phase transition can
therefore be reached in all regions with an initial temperature ≤15 K.

Results
========

Physics
    The gravitational stability of single- and multi-component fluids were
    studied using linear analysis, with special emphasis on phase transitions.
    The Jeans criterion for ideal gas fluids predicts a minimum scale at which
    a fluid is unstable, while fluids in a phase transition are gravitationally
    unstable at any scale, because compression does not increase pressure, but
    the condensed phase fraction. Using the inter-molecular Lennard- Jones
    potential, the virial equilibrium study shows that there is an unvirial-
    izable density domain where H₂ clumps can form at temperatures ≤ 600 K.

Simulations
    The non-linear dynamics of single-component and binary fluids in and out of
    a phase transition are studied using the molecular dynamics code `LAMMPS
    <http://lammps.sandia.gov>`_.  Super-molecules are used to combine the
    Lennard-Jones and gravitational potential. Test-simulations affirm the
    independence of the number of super-molecules and correctly reproduce ideal
    gas collapses in accordance with the Jeans criterion. The simulations
    confirm that fluids presenting a phase transition are gravitationally
    unstable, independent of the strength of the gravitational potential. The
    instabilities produce a wide spectrum of ice clumps, from small multimers,
    to comets, to gravitationally bound planetoids.  Instabilities in H₂-He
    fluids lead to the formation of gaseous He-planetoids if above the ideal
    gas Jeans criterion, or to the formation of rocky H₂-planetoids if below
    the Jeans criterion and if H₂ is in a phase transition.


Conclusions
    The work done so far shows that the physics of cold self-gravitation fluids
    such as dark molecular clouds is much richer than usually assumed. The
    segregation in a gravitational field of small grains towards larger bodies
    such as comets and planetoids cannot be simulated with traditional
    hydrodynamical codes, but is possible with a super-molecular approach.
    Observations, linear and virial analysis as well as computer simulations
    suggest the possibility of the formation of substellar H₂ bodies due to the
    combination of phase transition and gravity in cold regions, as fluids in a
    phase transition are gravitationally unstable, independent of the strength
    of the gravitational potential. H₂ phase transition is reached easily
    during plane-parallel collapses if the initial temperature is ≤15 K.

Current Work
============

Collapsing Geometry
    Our results show that once a selfgravitating fluid reaches phase transition
    conditions, solid H2 bodies form. The question is therefore, how to reach
    these conditions. Using `RAMSES
    <http://www.ics.uzh.ch/~teyssier/ramses/RAMSES.html>`_, I simulate
    plane-parallel collapses until they reach high density. The resulting
    conditions are then used as initial conditions for `LAMMPS
    <http://lammps.sandia.gov>`_ simulations.

Rotating Systems
    As soon as a collapsing system has an angular momentum ≠0, a disk will
    form. The study of rotating systems is of utmost interest in astrophysics,
    as they are ubiquitous. Rotating spheres with different density profiles
    are simulated with `LAMMPS <http://lammps.sandia.gov>`_ using a simple
    cooling algorithm. Due to the conservation of angular momentum, a disk
    forms. If the density is high enough, or the temperature low enough, the
    disk is in a phase transition and solid bodies are forming.
