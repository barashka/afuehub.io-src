====================
Interpolation Scheme
====================

:order: 4

**Smooth Cubic Multivariate Local Interpolations**

This is a software library which calculates cubic interpolations in up to four
dimensions for values on a grid. In order to calculate the coefficients of the
cubic polynom, only local values are used: The data itself and its first-order
derivatives. This is in contrast to splines, where the coefficients are not
calculated using derivatives, but non-local data, which can lead to
over-smoothing the result.

There exist two versions, one written in `Julia <http://julialang.org/>`_, one
in `C <https://gcc.gnu.org/>`_. Both versions are published on Github. I
developed the first version in Julia, both to test and learn this new and
promising programming language, but also to serve as a prototype for the shared
library which I developed in C.  The Julia version can be found on the
following link:

https://github.com/AFueglistaler/LocalInterpolations.jl

The second version is developed in C. It can be used as a static library in a
C/C++ project or as a shared library in projects using other programming
languages such as FORTRAN.

There is also a `Python <https://www.python.org/>`_-wrapper using `CTypes
<https://docs.python.org/2/library/ctypes.html>`_ to call the functions of the
shared library. I included mapper functions, which map the interpolation and/or
its derivative on a numpy-array, to boost the performance. The C version can be
found on the following link:

https://github.com/AFueglistaler/loci
