=================
About me
=================

:URL: 
:save_as: index.html
:slug: Andreas Füglistaler
:order: 1

Hi, I am a scientist from Switzerland. I have been working in astrophysics,
space technology and fluid dynamics and am interested in high performance
computing (HPC), programming and all types of computer simulations. 

During high school, I started working as a software engineer. I continued that
work during two years after finishing high school, before doing my studies at
the `EPFL <http://www.epfl.ch>`_ in mechanical engineering.  I specialized in
fluid dynamics and numerical methods and did a minor in space technologies.
After my studies, I worked two years in the industry: first as a space engineer
in collaboration with `RUAG Space
<https://www.ruag.com/en/products-services/space>`_, then I co-founded and
worked for the start-up called  `Ray Research <http://rayaircraft.com>`_ to
develop a vertical take-off and landing (VTOL) aircraft.

My curiosity for computational sciences led me to do a PhD in astrophysics at
the university of Geneva  under the guidance of Daniel Pfenniger. I studied the
gravitational stability of fluids in a phase transition analytically and with
numerical simulations on a computing cluster. I used the molecular dynamics
code `LAMMPS <http://lammps.sandia.gov>`_ which I modified to use in an
astrophysical context and developed scientific software using C/`C++
<https://isocpp.org/>`_, `Python <https://www.python.org/>`_ and `Julia
<http://julialang.org/>`_. After finishing my PhD, I continued work at the
university of Geneva as a computational scientist, where I deepen the topic of
my PhD thesis and develop a numerical library for local cubic interpolations in
up to four dimensions.

Documents
=========

+ `Curriculum Vitae <{filename}../pdfs/AndreasFueglistaler_CV.pdf>`_
+ `PhD Thesis <{filename}../pdfs/AndreasFueglistaler_PhD.pdf>`_ and `Poster <{filename}../pdfs/AndreasFueglistaler_PhDPoster.pdf>`_
+ `MSc Thesis <{filename}../pdfs/AndreasFueglistaler_MSc.pdf>`_ and `Poster <{filename}../pdfs/AndreasFueglistaler_MScPoster.pdf>`__


Publications
============

+ `ADS Author Query <http://adsabs.harvard.edu/cgi-bin/nph-abs_connect?return_req=no_params&author=F%C3%BCglistaler,%20Andreas>`_

:Email:       `andreas.fueglistaler@gmail.com <mailto:andreas.fueglistaler@gmail.com>`_ 
:GitHub:      `AFueglistaler <https://github.com/AFueglistaler>`_
